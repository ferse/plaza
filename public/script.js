/*Obtiene todos los elementos con la clase "componente", 
que serian las imagenes*/
let element = document.getElementsByClassName('componente');

//Ejecuta la funcion arrastrar por cada imagen
for(let i = 0; i<element.length; i++){
    arrastrar(element[i]);
};

function arrastrar(element){
    let deltaX = 0;
    let deltaY = 0;

    element.onpointerdown = agarrar;

    function agarrar(e){
        e.preventDefault();
        //Se guarda el width y height del elemento
        let ancho = element.width;
        let alto = element.height;
        /*Se asgina el mismo height y width 
        para que mantenga sus medidas al
        poner "position:absolute" */
        element.width = ancho;
        element.height = alto;
        deltaX = e.clientX - element.offsetLeft;
        deltaY = e.clientY - element.offsetTop;
        document.onpointermove = mover;
        document.onpointerup = soltar;
    }

    function mover(e){
        element.style.position = 'absolute';
        element.style.left = e.clientX - deltaX + 'px';
        element.style.top = e.clientY - deltaY + 'px';
    }

    function soltar(e){
        document.onpointermove = null;
        document.onpointerup = null;
    }
}